let tempo = 0
let wave = []

function setup() {
  createCanvas(windowWidth, windowHeight);

}

function draw() {
  background(0);
  translate(width/2,height/2)
  stroke(255)
  // line(0, -height/2, 0, height/2)

  let x = 0
  let y = 0

  // translate(-600, 0)
  for (let i = 0; i < 10; i++) {
    let prevx = x
    let prevy = y
    let frequencia = i * 1 + 1
    let raio = 150 * (4 / (frequencia * PI))
    // let raio = 75 * ( 8 * ((-((frequencia-1)/2)) / pow(frequencia, 2)) )
    x += raio * cos(frequencia * tempo)
    y += raio * sin(frequencia * tempo)
    
    noFill()
    stroke(255, 100)
    strokeWeight(2)
    // circle(prevx, prevy, 2*raio)
    stroke(255)
    // line(prevx, prevy, x, y)
    strokeWeight(5)
    // circle(x, y, 2)
  }

  stroke(255, 100)
  strokeWeight(2)
  // line(x, y, 300, wave[0])

  translate(-400, 0)
  wave.unshift(y)

  beginShape()
  stroke(255)
  strokeWeight(2)
  for (let i = 0; i < wave.length; i++) {
    vertex(i, wave[i])
  }
  endShape()

  if (wave.length > 800) {
    wave.pop()
  }

  
  tempo -= 0.02
}
