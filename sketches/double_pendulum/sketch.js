  var r1 = 100
  var r2 = 100
  var r3 = 100
  var m1 = 10
  var m2 = 10
  var m3 = 10
  var a1 = 0
  var a2 = 0
  var a3 = 0
  var track

  var prevX = 0
  var prevY = 0

function setup() {
  createCanvas(windowWidth, windowHeight);
  track = createGraphics(width, height)
  track.translate(width/2, height/2)
  angleMode(DEGREES)

  let trackBox = createCheckbox('track', false);
  trackBox.changed(enableTrack);
  // let penBox = createCheckbox('pendulum', false);
  // penBox.changed(myCheckedEvent);

}

function draw() {
  background(255);
  image(track, 0, 0)

  stroke(0)
  strokeWeight(2)
  fill(0)

  track.stroke(0)
  track.strokeWeight(2)
  track.noFill()

  translate(width/2, height/2)

  for(var i = 0; i < 1; i++) {
    var x1 = r1 * sin(a1)
    var y1 = r1 * cos(a1)
    var x2 = x1 + r2 * sin(a2)
    var y2 = y1 + r2 * cos(a2)
    var x3 = x2 + r3 * sin(a3)
    var y3 = y2 + r3 * cos(a3)

    if(trackBox) {
      line(0, 0, x1, y1)
      circle (x1, y1, m1)
      line(x1, y1, x2, y2)
      circle (x2, y2, m2)
      line(x2, y2, x3, y3)
      circle (x3, y3, m3)
    }

    // track.circle(x3, y3, 5)

    if(frameCount > 1) {
      track.line(prevX, prevY, x3, y3)
    }
    prevX = x3
    prevY = y3

    changeAng()
  }

}

function changeAng() {
  a1 += sin(a2) + cos(a3)
  a2 -= 5
  a3 += 1
}

function enableTrack() {
  if (this.checked()) {
    console.log('Checking!');
  } else {
    console.log('Unchecking!');
  }
}