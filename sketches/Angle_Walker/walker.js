class Walker {

    constructor(name=null, _x=0, _y=0) {
        this.trail = []
        this.loc0 = new createVector (_x, _y)
        this.loc = this.loc0
        this.vel = new createVector (0, 0)
        this.acc = new createVector (0, 0)
        this.maxSpeed = 10
        this.time = new createVector (0, 10000)
        this.alpha = 255
        this.objectName = name

        this.mouse = createVector(mouseX, mouseY)
        this.a = createVector(0,0)
        this.b = createVector(100,0)

        this.borda = 400

        this.writer = createWriter('trail.txt')
        this.saved = false
    }

    update() {
        this.accelerate()
        // this.randomVel()

        this.vel.add(this.acc)
        this.vel.limit(this.maxSpeed)
        this.loc.add(this.vel)

        this.mouse.set(mouseX, mouseY)

        // this.edgeLoop()
        // this.edgeStop()
        // this.edgeReflect()
        this.edgeReflectRadius()

        // print(this.loc.dist(this.loc0),"loc0: ", this.a, "loc ", this.loc)
        // print(this.loc.angleBetween(this.loc0))

        if(frameCount % 2 === 0) {
            this.trail.push({x: this.loc.x, y:this.loc.y})
        }

        // print("trail: ", this.trail)
        // if(frameCount > 120 && this.saved === false) {
        //     this.writer.print("frame: ", this.trail)
        //     this.writer.close()
        //     this.writer.clear()
        //     this.saved = true
        // }

        // if(frameCount === 120) {
        //     noLoop()
        // }
    }

    display() {
        // fill(0)
        stroke(0)
        strokeWeight(2)
        strokeCap(ROUND)
        strokeJoin(ROUND)
        noFill()
        // noStroke()

        beginShape()
        this.trail.forEach (p => {
            vertex(p.x, p.y)
        })
        endShape()
    }

    accelerate() {
        this.acc.set(random(-1, 1), random(-1, 1))

        // this.acc.set(1, 1)
        // // this.acc.setHeading(this.loc.angleBetween(this.mouse))
        // print(this.loc.angleBetween(this.mouse), this.loc, this.mouse)
    }

    randomVel() {
        this.vel.set(random(-2, 2), random(-2, 2))
    }

    edgeLoop() {
        if(this.loc.x > width/2 - this.borda) {
            this.loc.x = -width/2 + this.borda
        } else if(this.loc.x < -width/2 + this.borda) {
            this.loc.x = width/2 - this.borda
        }

        if(this.loc.y > height/2 - this.borda) {
            this.loc.y = -height/2 + this.borda
        } else if(this.loc.y < -height/2 + this.borda) {
            this.loc.y = height/2 + this.borda
        }
    }

    edgeStop() {
        if(this.loc.x > width/2) {
            this.loc.x = width/2
        } else if(this.loc.x < -width/2) {
            this.loc.x = -width/2
        }

        if(this.loc.y > height/2) {
            this.loc.y = height/2
        } else if(this.loc.y < -height/2) {
            this.loc.y = -height/2
        }
    }

    edgeReflect() {
        if(this.loc.x > width/2 - this.borda) {
            this.loc.x = width/2 - this.borda
            this.vel.x = this.vel.x * -1
        } else if(this.loc.x < -width/2 + this.borda) {
            this.loc.x = -width/2 + this.borda
            this.vel.x = this.vel.x * -1
        }

        if(this.loc.y > height/2 - this.borda) {
            this.loc.y = height/2 - this.borda
            this.vel.y = this.vel.y * -1
        } else if(this.loc.y < -height/2 + this.borda) {
            this.loc.y = -height/2 + this.borda
            this.vel.y = this.vel.y * -1
        }
    }

    edgeReflectRadius() {
        if(this.loc.dist(this.a) > this.borda ) {
            // this.loc.x = this.borda * cos(this.loc.angleBetween(this.loc0))
            // this.loc.y = this.borda * sin(this.loc.angleBetween(this.loc0))
            // print("ok")
            this.vel.mult(-1)
        }
    }
}