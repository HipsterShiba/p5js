mouseX = 0
mouseY = 0
var prevX = mouseX
var prevY = mouseY

function preload() {
  UIsetup()
}

function setup() {
  createCanvas(windowWidth, windowHeight)
  background(255)
  // frameRate(60)

}

function draw() {
  brush(0)
  
  UIdisplay()
}

function keyTyped() {
  // reset shortcut
  if (key == 'r') {
    background (255)
  }
}

function bg() {
  background(255)
}

let brushSize = 5

function brush(brushColor) {
  if (mouseIsPressed) {
    stroke(brushColor)
    strokeWeight(brushSize)
    line(prevX,prevY,mouseX,mouseY)
  }
  prevX = mouseX
  prevY = mouseY
}

function mouseWheel(event) {
  brushSize -= (event.delta/50)
  if (brushSize < 5) {
    brushSize = 5
  } else if (brushSize > 300) {
    brushSize = 300
  }
}

function UIsetup() {
  // reset
  resetB = createButton("RESET")
  resetB.class("resetButton")
  resetB.style("background-color:#eb872b")
  resetB.position(50, 50)

  // color Swacth

  // black
  var black = "#000000"
  var radius = "25%"
  blackB = createButton("")
  blackB.style("background-color:" + black)
  blackB.style("border-radius:" + radius)
  blackB.position(50, 140)

}

function UIdisplay() {
  // reset
  resetB.mouseReleased(bg)
  resetB.mouseOut(mouseIsOutRB)
  resetB.mouseOver(mouseIsOverRB)

  // color Swatch

  // black
  blackB.mouseReleased(bg)
  blackB.mouseReleased(swatchSelect)
  blackB.class("swatch")
  // blackB.style("border-radius:" + radius)
  // blackB.mouseOut(mouseIsOut)
  // blackB.mouseOver(mouseIsOver)
}

function mouseIsOutRB() {
  resetB.removeClass("resetButtonMO")
  resetB.class("resetButton")
}

function mouseIsOverRB() {
  resetB.removeClass("resetButton")
  resetB.class("resetButtonMO")
}

function swatchSelect() {
  radius = "50%"
  print("black swatch clicked")
}