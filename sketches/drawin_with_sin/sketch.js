var x = 0
var y = 0
var prevX = 0
var prevY = 0

var amplitudeX = 40
var amplitudeY = 500
var frequenciaX = [440]
var frequenciaY = 1
var periodo = 60
var anguloX = []
var anguloY = -90

var frame =  0
var tempo = 0
const fps = 60

var skipFrameOne = 0

function setup() {
  createCanvas(windowWidth, windowHeight);

  fill(0)
  noStroke()
  background(220);
  frameRate(fps)

  for(let i = 0; i < 4; i++) {
    // frequenciaX[i] = random(440)
  }
  
  for (let i = 0; i < frequenciaX.length; i++) {
    anguloX[i] = -90
  }

  console.log(frequenciaX)

}

function draw() {
  
  translate(width/2,height/2)

  // screenRefresh()
  // contadorDeTempo()
  // redCross()

  // drawGraph()
  drawFullGraph()
}

function screenRefresh() {
  if (frame >= periodo * fps) {
    background(220)
    frame = 0
    tempo = 0
  }
}
function contadorDeTempo() {
  push()
  fill(220)
  rect (-width/2, -height/2, 200, 1000)
  pop()

  frame++
  if (frame%60 == 0) {
    tempo++
  }

  const margem = 20
  textSize(32)
  text(tempo, -width/2+margem, -height/2+margem+34)
  text(frame, -width/2+margem, -height/2+margem+34*2)
}
function redCross() {
  push()
  noFill()
  stroke(255, 0, 0)
  strokeWeight(2)
  line(-width, 0, width, 0)
  line(0, -height, 0, height)
  pop()
}
function drawGraph() {
  
  // circle(0, 0, 5, 5)
  // circle(x, y, 5, 5)

  for (let i = 0; i < frequenciaX.length; i++) {
    y += cos(radians(anguloX[i])) * (cos(anguloX[0])*amplitudeX)
  }
  x = sin(radians(anguloY)) * amplitudeY

  if(skipFrameOne < 1) {
    prevX = x
    prevY = y
    skipFrameOne = 1
  }

  push()
  stroke(0)
  strokeWeight(2)
  noFill()
  line(x, y, prevX, prevY)
  pop()

  prevX = x
  prevY = y

  for (let i = 0; i < frequenciaX.length; i++) {
    anguloX[i] += (frequenciaX[i]*360)/(periodo*fps)
  }
  anguloY += (frequenciaY*360)/(periodo*fps)
}
function drawFullGraph() {
  while (anguloY <= 275) {
    drawGraph()
  }
}

