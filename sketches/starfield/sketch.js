var star = [];
var numStars = 10000;

function setup() {
  createCanvas(windowWidth, windowHeight);

  for (let i = 0; i < numStars; i++) {
    star[i] = new Star();
}

}

function draw() {
  background(0);
  translate(width/2, height/2);

  for (let i = 0; i < numStars; i++) {
    star[i].update();
    // star[i].displayC();
    star[i].displayS();
  }
  
}
