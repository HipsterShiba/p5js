class Star {

    constructor() {
        this.x = random(-width, width);
        this.y = random(-height, height);
        this.z = random(width);
        this.pz = this.z;
    }

    update() {
        this.z -= map(mouseX, 0, width, 0, 100);
        if (this.z < 1) {
            this.z = width;
            this.x = random(-width, width);
            this.y = random(-height, height);
            this.pz = this.z
        }
        
    }

    displayC() {
        var sx = map(this.x/this.z, 0, 1, 0, width);
        var sy = map(this.y/this.z, 0, 1, 0, height);
        var size = map(this.z, 0, width, 16, 0);

        noStroke();
        fill(255);

        circle (sx-1, sy-1, size/2);
        circle (sx, sy, size);
    }

    displayS() {
        var sx = map(this.x/this.z, 0, 1, 0, width);
        var sy = map(this.y/this.z, 0, 1, 0, height);
        var px = map(this.x/this.pz, 0, 1, 0, width);
        var py = map(this.y/this.pz, 0, 1, 0, height);

        var alpha = map(this.z, 0, width, 255, 0);

        stroke(255,255,255,alpha);
        strokeWeight(2);
        noFill();

        line (px, py, sx, sy)

        this.pz = this.z;
    }

}